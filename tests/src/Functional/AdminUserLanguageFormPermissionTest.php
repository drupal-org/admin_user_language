<?php

namespace Drupal\Tests\admin_user_language\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests BasicForm to alter admin_user_language configuration.
 *
 * @coversDefaultClass \Drupal\admin_user_language\Form\BasicForm
 *
 * @group admin_user_language
 */
class AdminUserLanguageFormPermissionTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['user', 'admin_user_language'];

  /**
   * Tests the basic functionality of the field.
   */
  public function testPermissionDenied() {
    $admin_user = $this->drupalCreateUser(['access administration pages']);
    $this->drupalLogin($admin_user);

    // Display settings form.
    $output = $this->drupalGet('admin/config/admin_user_language/settings');

    $this->assertStringContainsString('Access denied', $output);
  }

  /**
   * Tests the basic functionality of the field.
   */
  public function testPermissionGranted() {
    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer admin interface language',
    ]);
    $this->drupalLogin($admin_user);

    // Display settings form.
    $string = $this->drupalGet('admin/config/admin_user_language/settings');
    $this->assertStringNotContainsString('Access denied', $string);
  }

}
